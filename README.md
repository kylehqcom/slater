# slater

_slater_ extends the [https://gitlab.com/kylehqcom/slate](https://gitlab.com/kylehqcom/slate/) package to allow submitting a `slate.ScheduleEntry` at a given time. On create, you are returned a read only channel to determine the response of `slate.ScheduleEntry` submission.

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/kylehqcom/slater)](https://goreportcard.com/report/gitlab.com/kylehqcom/slater)

---

## Install

First add the package with `go get gitlab.com/kylehqcom/slater`

```go
import (
    "gitlab.com/kylehqcom/slate"
    "gitlab.com/kylehqcom/slater"
)

s := slate.NewSchedule()
slater, err := slater.NewSubmitter(s, slater.WithSubmitAtTickDuration(time.Second*10)) // Defaults time.Second
if err != nil {
    // handle
}

func Simple() {
    // Do some repeated work.
}

e := slate.ScheduleEntry{
    Fn:    Simple,
    Every: time.Duration(time.Second),
}
at := time.Now().Add(time.Second*300) // Submit in 5mins time
slater.SubmitAt(e, at)

for {
    select {
    case r := <-slater.Response:
        if r.Error != nil {
            // handle
        }
        e := r.ScheduleEntry
    }
}
```

## General Usage

The normal _Tick_ duration to check for _At_ submission is every second. In most cases this will be perfectly fine but you should note that you are free to alter as you require. Just be aware that you can get unexpected results if you assign a tick of every 24 hours for example and submit jobs with Every values of a second. With great power...

You can call `slater.Stop()` to cease all submission processing and will close the `slater.Response` channel. As a general FYI, many callers fall into the trap of submitting schedules on the hour, Eg at midnight. But the vast majority of processing doesn't need to be done on the hour at all. What is normally required is that you need a defined period of time, eg the last 24 hours ending at midnight. So should you require to process "sales" for the last 24 hours, you can normally delay the processing for some time after the period end.

If you like this package or are using for your own needs, then _star_\* and let me know via [https://twitter.com/kylehqcom](https://twitter.com/kylehqcom)
