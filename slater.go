package slater

import (
	"fmt"
	"sort"
	"sync"
	"time"

	"gitlab.com/kylehqcom/slate"
)

type (
	// Submitter is the interface used to manipulate submissions
	Submitter interface {
		// SubmitAt will submit a slate.ScheduleEntry at a given time
		SubmitAt(e slate.ScheduleEntry, at time.Time)
	}

	// Slater is this packages implementation of the Submitter interface
	Slater struct {
		Response chan SubmitterResponse
		Schedule *slate.Schedule
		Stopper  chan struct{}
		Ticker   *time.Ticker
		internal slaterInternals
	}

	// Note that we use a map to array of slate entries so that entries submitted
	// for the same time are handled. Most callers expect "midnight" processing for
	// example (which is normally a ruse).
	slaterInternals struct {
		stack map[int64][]slate.ScheduleEntry
		order []int64
		sync.Mutex
	}

	// SubmitterOptions hold the options for submitters to process a submissions
	SubmitterOptions struct {
		TickDuration time.Duration
	}

	// SubmitterOption funcs are used to bind SubmitterOptions
	SubmitterOption func(*SubmitterOptions)

	// SubmitterResponse is used to return the outcome of a Submitter call via a channel
	SubmitterResponse struct {
		Error         error
		ScheduleEntry slate.ScheduleEntry
	}
)

// NewSubmitter creates a Submitter interface to return Submitter Responses
func NewSubmitter(s *slate.Schedule, opts ...SubmitterOption) (*Slater, error) {
	if s == nil {
		return nil, fmt.Errorf("Nil Schedule provided on NewSubmitter")
	}

	sopts := NewSubmitterOptions()
	for _, opt := range opts {
		opt(&sopts)
	}

	sltr := &Slater{
		Response: make(chan SubmitterResponse),
		Schedule: s,
		Stopper:  make(chan struct{}),
		Ticker:   time.NewTicker(sopts.TickDuration),
		internal: slaterInternals{
			stack: map[int64][]slate.ScheduleEntry{},
			order: []int64{},
		}}

	// Kickoff the ticker
	go sltr._tick()
	return sltr, nil
}

// NewSubmitterOptions will return a SubmitterOptions struct with defaults assigned
func NewSubmitterOptions() SubmitterOptions {
	return SubmitterOptions{
		TickDuration: time.Second,
	}
}

// _doSubmit submits the entry to the schedule - sending the response down the Response chan
func (s *Slater) _doSubmit(e slate.ScheduleEntry) {
	ID, err := s.Schedule.Submit(e)
	e.ID = ID // Bind the ID from submission process
	s.Response <- SubmitterResponse{
		Error:         err,
		ScheduleEntry: e,
	}
}

// Stop will close the stopper channel to break from submissions
func (s *Slater) Stop() {
	close(s.Stopper)
}

// SubmitAt will submit a slate.ScheduleEntry "At" a given time to this submitter.
func (s *Slater) SubmitAt(e slate.ScheduleEntry, at time.Time) {
	// Normalize to UTC for processing
	now := time.Now().UTC()
	at = at.UTC()

	// If "at" is in the past we can submit instantly with no other processing.
	if at.Before(now) {
		go s._doSubmit(e)
		return
	}

	key := at.UnixNano()
	s.internal.Lock()
	defer s.internal.Unlock()

	// Append the new entry and sort the internal order
	s.internal.stack[key] = append(s.internal.stack[key], e)
	s.internal.order = append(s.internal.order, key)
	sort.Slice(s.internal.order, func(i, j int) bool { return s.internal.order[i] < s.internal.order[j] })
}

// _tick is responsible for checking new entries to submit. Should be called in a non blocking goroutine.
func (s *Slater) _tick() {
	defer s.Ticker.Stop()
	for {
		select {
		case t := <-s.Ticker.C:
			var cursor int64
			tick := t.UTC().UnixNano()
			s.internal.Lock()
			for _, i := range s.internal.order {
				// If the ordered entry timestamp is greater than this tick, we can break immediately.
				// Only entries that are less than or equal to this tick timestamp value should be processed.
				if i > tick {
					break
				}

				if entries, ok := s.internal.stack[i]; ok {
					for _, e := range entries {
						go s._doSubmit(e)
					}
				}
				// With entries submitted, drop from the stack
				delete(s.internal.stack, i)
				cursor++
			}

			// Reset the internal order array. As we know it's already in sequence, we can
			// replace from the cursor value which is the index of the internal order array. We
			// do not need the != 0 check, but that should be faster than leaving as zero and
			// re-spreading the non changed internal order array.
			if cursor != 0 {
				s.internal.order = s.internal.order[cursor:]
			}

			// Unlock the mutex now we have processed.
			s.internal.Unlock()

		case <-s.Stopper:
			return
		}
	}
}

// WithSubmitAtTickDuration will bind the time.Duration to the default SubmitAt options
func WithSubmitAtTickDuration(tick time.Duration) SubmitterOption {
	return func(s *SubmitterOptions) {
		s.TickDuration = tick
	}
}
