module gitlab.com/kylehqcom/slater

go 1.14

require (
	github.com/matryer/is v1.3.0
	gitlab.com/kylehqcom/slate v0.1.0
)
