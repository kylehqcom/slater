package slater

import (
	"math/rand"
	"testing"
	"time"

	"github.com/matryer/is"
	"gitlab.com/kylehqcom/slate"
)

func TestNewSubmitter(t *testing.T) {
	is := is.New(t)
	_, err := NewSubmitter(nil)
	is.True(err != nil)
}

func TestSubmitAtOrder(t *testing.T) {
	is := is.New(t)
	slater, err := NewSubmitter(slate.NewSchedule())
	is.NoErr(err)
	defer slater.Stop()

	// Create an array of atTimes, then randomize and check that Slater as ordered correctly.
	now := time.Now()
	atTimes := []time.Time{}
	ordered := []int64{}
	for i := 1; i < 10; i++ {
		offset := time.Duration(i*50) * time.Second
		atTime := now.Add(offset)
		atTimes = append(atTimes, atTime)
		ordered = append(ordered, atTime.UnixNano())
	}

	atRandom := make([]time.Time, len(atTimes))
	copy(atRandom, atTimes)

	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(atRandom), func(i, j int) { atRandom[i], atRandom[j] = atRandom[j], atRandom[i] })

	// With the ordered and random ats in place, submit every random and
	// then assert that the internal slater order matches the expected ordered.
	e := slate.ScheduleEntry{}
	for _, a := range atRandom {
		slater.SubmitAt(e, a)
	}
	is.Equal(ordered, slater.internal.order)
}

func TestSubmitAt(t *testing.T) {
	is := is.New(t)
	slater, err := NewSubmitter(slate.NewSchedule(), WithSubmitAtTickDuration(time.Millisecond*50))
	is.NoErr(err)

	e := slate.ScheduleEntry{
		Fn:    func() {},
		Every: time.Hour,
	}
	time120 := time.Now().Add(time.Millisecond * 120)
	time170 := time.Now().Add(time.Millisecond * 170)
	slater.SubmitAt(e, time120)
	slater.SubmitAt(e, time120)
	slater.SubmitAt(e, time170)

	// Check the internals
	slater.internal.Lock()
	entries, ok := slater.internal.stack[time120.UTC().UnixNano()]
	slater.internal.Unlock()
	is.True(ok)
	if !ok {
		is.Fail() // We do not have a stack with the correct unix nano key
	}
	is.Equal(2, len(entries)) // Should be x2 entries for 120 milliseconds

	slater.internal.Lock()
	entries, ok = slater.internal.stack[time170.UTC().UnixNano()]
	slater.internal.Unlock()
	is.True(ok)
	if !ok {
		is.Fail() // We do not have a stack with the correct unix nano key
	}
	is.Equal(1, len(entries)) // Should be x1 entries for 170 milliseconds

	expects := []int64{time120.UTC().UnixNano(), time120.UTC().UnixNano(), time170.UTC().UnixNano()}
	slater.internal.Lock()
	is.Equal(slater.internal.order, expects) // Expected order does not equal
	slater.internal.Unlock()

	responseCount := 0

out:
	for {
		select {
		case r := <-slater.Response:
			if r.Error != nil {
				is.Fail() // Should not have an error submitting an ScheduleEntry
			}

			if r.ScheduleEntry.ID == "" {
				is.Fail() // ScheduleEntry should have an EntryID on submission
			}
			responseCount++

		case <-time.NewTicker(time.Millisecond * 180).C:
			slater.Stop()
			break out
		}
	}

	is.Equal(3, responseCount)
}

func TestSubmitAtInPast(t *testing.T) {
	is := is.New(t)
	slater, err := NewSubmitter(slate.NewSchedule())
	is.NoErr(err)
	e := slate.ScheduleEntry{ID: "foo"}
	slater.SubmitAt(e, time.Now().Add(-time.Second))
	res := <-slater.Response
	is.True(res.Error != nil)
	is.Equal(e, res.ScheduleEntry)
}
